use std::error::Error;

#[cfg(feature = "async")]
use std::{ future::Future, pin::Pin };

#[derive(Clone)]
pub struct Command<T> {
    /// The function pointer which this links to.
    pub command: CommandType<T>,
    /// A help string, should be less than 80 characters. For example, if it
    /// was an `echo` command:
    /// ```txt
    /// prints the arguments to the output.
    /// ```
    pub help: String,
}

impl<T> Command<T> {
    pub fn new(help: String, command: CommandFn<T>) -> Self {
        Self { command: CommandType::Sync(command), help }
    }

    #[cfg(feature = "async")]
    pub async fn new_async(help: String, command: AsyncCommandFn<T>) -> Self {
        Self { command: CommandType::Async(command), help }
    }
}

pub type CommandFn<T> = fn(&mut T, Vec<String>) -> Result<(), Box<dyn Error>>;

#[cfg(feature = "async")]
pub type AsyncCommandFn<T> = fn(&mut T, Vec<String>) -> Pin<Box<dyn Future<Output = Result<(), Box<dyn Error>>> + Send + '_ >>;

/// Command type specifies what type of command this is, namely wether it
/// is async or not.
#[derive(Clone)]
pub enum CommandType<T> {
    Sync(CommandFn<T>),
    #[cfg(feature = "async")]
    Async(AsyncCommandFn<T>),
}
