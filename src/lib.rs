//! # Shellfish
//! 
//! Shellfish is a library to include interactive shells within a program. This may be useful when building terminal application where a persistent state is needed, so a basic cli is not enough; but a full tui is over the scope of the project. Shellfish provides a middle way, allowing interactive command editing whilst saving a state that all commands are given access to.
//! 
//! ## The shell
//! 
//! By default the shell contains only 3 built-in commands:
//! 
//!  * `help` - displays help information.
//!  * `quit` - quits the shell.
//!  * `exit` - exits the shell.
//! 
//! The last two are identical, only the names differ.
//! 
//! When a command is added by the user (see bellow) the help is automatically generated and displayed. Keep in mind this help should be kept rather short, and any additional help should be through a dedicated help option.
//! 
//! ## Example
//! 
//! The following code creates a basic shell, with the added commands:
//!  * `greet`, greets the user.
//!  * `echo`, echoes the input.
//!  * `count`, increments a counter.
//! 
//! Also, if run with arguments than the shell is run non-interactvely.
//! 
//! ```rust
//! use shellfish::{app, Command, Shell};
//! use std::convert::TryInto;
//! use std::error::Error;
//! use std::fmt;
//! use std::ops::AddAssign;
//! 
//! fn main() -> Result<(), Box<dyn std::error::Error>> {
//!     // Define a shell
//!     let mut shell = Shell::new(0_u64, "<[Shellfish Example]>-$ ");
//! 
//!     // Add some commands
//!     shell.commands.insert(
//!         "greet".to_string(),
//!         Command::new("greets you.".to_string(), greet),
//!     );
//! 
//!     shell.commands.insert(
//!         "echo".to_string(),
//!         Command::new("prints the input.".to_string(), echo),
//!     );
//! 
//!     shell.commands.insert(
//!         "count".to_string(),
//!         Command::new("increments a counter.".to_string(), count),
//!     );
//! 
//!     // Check if we have > 2 args, if so no need for interactive shell
//!     let mut args = std::env::args();
//!     if args.nth(1).is_some() {
//!         // Create the app from the shell.
//!         let mut app: app::App<u64, app::DefaultCommandLineHandler> =
//!             shell.try_into()?;
//! 
//!         // Set the binary name
//!         app.handler.proj_name = Some("shellfish-example".to_string());
//!         app.load_cache()?;
//! 
//!         // Run it
//!         app.run_args()?;
//!     } else {
//!         // Run the shell
//!         shell.run()?;
//!     }
//!     Ok(())
//! }
//! 
//! /// Greets the user
//! fn greet(_state: &mut u64, args: Vec<String>) -> Result<(), Box<dyn Error>> {
//!     let arg = args.get(1).ok_or_else(|| Box::new(GreetingError))?;
//!     println!("Greetings {}, my good friend.", arg);
//!     Ok(())
//! }
//! 
//! /// Echos the input
//! fn echo(_state: &mut u64, args: Vec<String>) -> Result<(), Box<dyn Error>> {
//!     let mut args = args.iter();
//!     args.next();
//!     for arg in args {
//!         print!("{} ", arg);
//!     }
//!     println!();
//!     Ok(())
//! }
//! 
//! /// Acts as a counter
//! fn count(state: &mut u64, _args: Vec<String>) -> Result<(), Box<dyn Error>> {
//!     state.add_assign(1);
//!     println!("You have used this counter {} times", state);
//!     Ok(())
//! }
//! 
//! /// Greeting error
//! #[derive(Debug)]
//! pub struct GreetingError;
//! 
//! impl fmt::Display for GreetingError {
//!     fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
//!         write!(f, "No name specified")
//!     }
//! }
//! 
//! impl Error for GreetingError {}
//! 
//! ```
#![deny(rustdoc::broken_intra_doc_links)]

use std::collections::HashMap;
use std::fmt::Display;
use std::io;
use std::io::prelude::*;

pub mod command;
pub use command::Command;

pub mod handler;
pub use handler::Handler;
#[cfg(feature = "async")]
pub use handler::AsyncHandler;

#[cfg(feature = "app")]
pub mod app;
#[cfg(feature = "app")]
pub use app::App;

#[cfg(feature = "rustyline")]
use rustyline::{error::ReadlineError, Editor};

use yansi::Paint;

/// A shell represents a shell for editing commands in.
///
/// Here are the generics:
///  * T: The state.
///  * M: The prompt. Can be anything that can be printed.
///  * H: The handler. Should implement either `Handler` or `AsyncHandler`,
///    or no functionality is present.
#[derive(Clone)]
pub struct Shell<T, M: Display, H> {
    /// The shell prompt.
    ///
    /// It can be anything which implements Display and can therefore be
    /// printed (This allows for prompts that change with the state.)
    pub prompt: M,
    /// This is a list of commands for the shell. The hashmap key is the
    /// name of the command (ie `"greet"`) and the value is a wrapper
    /// to the function it corresponds to (as well as help information.)
    pub commands: HashMap<String, Command<T>>,
    /// This is the state of the shell. This stores any values that you
    /// need to be persisted over multiple shell commands. For example
    /// it may be a simple counter or maybe a session ID.
    pub state: T,
    /// This is the handler for commands. See the [`Handler`](crate::Handler)
    /// documentation for more.
    pub handler: H,
    /// This is the description of the shell as a whole. This is displayed when
    /// requesting help information.
    pub description: String,
}

impl<T, M: Display> Shell<T, M, handler::DefaultHandler> {
    /// Creates a new shell
    pub fn new(state: T, prompt: M) -> Shell<T, M, handler::DefaultHandler> {
        Shell {
            prompt,
            commands: HashMap::new(),
            state,
            handler: handler::DefaultHandler(),
            description: String::new(),
        }
    }
}

#[cfg(feature = "async")]
impl<T, M: Display> Shell<T, M, handler::DefaultAsyncHandler> {
    /// Creates a new shell
    pub fn new_async(state: T, prompt: M) -> Self {
        Shell {
            prompt,
            commands: HashMap::new(),
            state,
            handler: handler::DefaultAsyncHandler(),
            description: String::new(),
        }
    }
}

impl<T, M: Display, H: Handler<T>> Shell<T, M, H> {
    /// Creates a new shell with the given handler.
    pub fn new_with_handler(state: T, prompt: M, handler: H) -> Self {
        Shell {
            prompt,
            commands: HashMap::new(),
            state,
            handler,
            description: String::new(),
        }
    }

    /// Starts running the shell
    pub fn run(&mut self) -> Result<(), Box<dyn std::error::Error>> {
        // Get the stdin & stdout.
        #[cfg(not(feature = "rustyline"))]
        let stdin = io::stdin();
        #[cfg(feature = "rustyline")]
        let mut rl = Editor::<()>::new();
        let mut stdout = io::stdout();

        '_shell: loop {
            // Display the prompt
            print!("{}", self.prompt);
            stdout.flush()?;

            // Read a line
            #[cfg(not(feature = "rustyline"))]
            let mut line = String::new();
            #[cfg(feature = "rustyline")]
            let line;

            #[cfg(not(feature = "rustyline"))]
            {
                stdin.read_line(&mut line)?;
            }
            #[cfg(feature = "rustyline")]
            {
                let readline = rl.readline(&self.prompt.to_string());
                match readline {
                    Ok(rl_line) => {
                        rl.add_history_entry(&rl_line);
                        line = rl_line;
                    }
                    Err(ReadlineError::Interrupted) => continue '_shell,
                    Err(ReadlineError::Eof) => break '_shell,
                    Err(err) => return Err(Box::new(err)),
                }
            }

            // Runs the line
            match Self::unescape(line.trim()) {
                Ok(line) => {
                    if self.handler.handle(
                        line,
                        &self.commands,
                        &mut self.state,
                        &self.description,
                    ) {
                        break '_shell;
                    }
                }
                Err(e) => eprintln!("{}", Paint::red(e.as_str())),
            }
        }
        Ok(())
    }
}

#[cfg(feature = "async")]
impl<T: Send, M: Display, H: AsyncHandler<T>> Shell<T, M, H> {
    /// Creates a new shell with the given handler.
    pub fn new_with_async_handler(state: T, prompt: M, handler: H) -> Self {
        Shell {
            prompt,
            commands: HashMap::new(),
            state,
            handler,
            description: String::new(),
        }
    }

    /// Starts running the shell
    pub async fn run_async(&mut self) -> Result<(), Box<dyn std::error::Error>> {
        // Get the stdin & stdout.
        #[cfg(not(feature = "readline"))]
        cfg_if::cfg_if! {
            if #[cfg(features = "async_std")] {
                let stdin = async_std::io::stdin();
            } else if #[cfg(features = "tokio")] {
                let stdin = tokio::io::stdin();
            } else {
                let stdin = io::stdin();
            }
        }

        #[cfg(feature = "rustyline")]
        let mut rl = Editor::<()>::new();

        cfg_if::cfg_if! {
            if #[cfg(features = "async_std")] {
                let mut stdout = async_std::io::stdout();
            } else {
                let mut stdout = io::stdout();
            }
        }

        '_shell: loop {
            // Display the prompt
            print!("{}", self.prompt);
            cfg_if::cfg_if! {
                if #[cfg(features = "tokio")] {
                    tokio::spawn(async { stdout.flush().await? })?;
                } else if #[cfg(feature = "async_std")] {
                    async_std::task::spawn(async { stdout.flush().await? })?;
                } else {
                    stdout.flush()?;
                }
            }
            // Read a line
            #[cfg(not(feature = "rustyline"))]
            let mut line = String::new();
            #[cfg(feature = "rustyline")]
            let line;

            #[cfg(not(feature = "rustyline"))]
            cfg_if::cfg_if! {
                if #[cfg(feature = "async_std")] {
                    stdin.read_line(&mut line).await?;
                } else {
                    stdin.read_line(&mut line)?;
                }
            }
            #[cfg(feature = "rustyline")]
            {
                let readline = rl.readline(&self.prompt.to_string());
                match readline {
                    Ok(rl_line) => {
                        rl.add_history_entry(&rl_line);
                        line = rl_line;
                    }
                    Err(ReadlineError::Interrupted) => continue '_shell,
                    Err(ReadlineError::Eof) => break '_shell,
                    Err(err) => return Err(Box::new(err)),
                }
            }

            // Runs the line
            match Self::unescape(line.trim()) {
                Ok(line) => {
                    if self.handler.handle_async(
                        line,
                        &self.commands,
                        &mut self.state,
                        &self.description,
                    ).await {
                        break '_shell;
                    }
                }
                Err(e) => eprintln!("{}", Paint::red(e.as_str())),
            }
        }
        Ok(())
    }
}

impl<T, M: Display, H> Shell<T, M, H> {
    /// Unescapes a line and gets the arguments.
    fn unescape(command: &str) -> Result<Vec<String>, String> {
        // Create a vec to store the split int.
        let mut vec = vec![String::new()];

        // Are we in an escape sequence?
        let mut escape = false;

        // Are we in a string?
        let mut string = false;

        // Go through each char in the string
        for c in command.chars() {
            let segment = vec.last_mut().unwrap();
            if escape {
                match c {
                    '\\' => segment.push('\\'),
                    ' ' if !string => segment.push(' '),
                    'n' => segment.push('\n'),
                    'r' => segment.push('\r'),
                    't' => segment.push('\t'),
                    '"' => segment.push('"'),
                    _ => {
                        return Err(format!(
                            "Error: Unhandled escape sequence \\{}",
                            c
                        ))
                    }
                }
                escape = false;
            } else {
                match c {
                    '\\' => escape = true,
                    '"' => string = !string,
                    ' ' if string => segment.push(c),
                    ' ' if !string => vec.push(String::new()),
                    _ => segment.push(c),
                }
            }
        }

        if vec.len() == 1 && vec[0].is_empty() {
            vec.clear();
        }

        Ok(vec)
    }
}
