//! # App
//!
//! Apps allow you to create commamd line argument parsing for your shellfish
//! commands. Basically, you define your commands as normal and call
//! [`.run_args`](App::run_args) on app. State is also saved, and only
//! deleted when given `exit` or `quit`.
//!
//! **Note: Normal handlers won't work as they MUST assume that
//! the first argument is that of the binaries name.**

use crate::*;
pub use crate::handler::app::{CommandLineHandler, DefaultCommandLineHandler};
use serde::{Deserialize, Serialize};
use std::convert::TryFrom;
use std::env;
use std::error::Error;
use std::fs;

/// See the module level dicumentation. Note `App` closely mirrors state and
/// so can be created from it (given the right trait bounds)
pub struct App<
    T: Serialize + for<'a> Deserialize<'a>,
    H: Handler<T> + CommandLineHandler,
> {
    pub commands: HashMap<String, Command<T>>,
    pub state: T,
    pub handler: H,
    pub description: String,
}

impl<T: Serialize + for<'a> Deserialize<'a>, M: Display, H: Handler<T>>
    TryFrom<Shell<T, M, H>> for App<T, DefaultCommandLineHandler>
{
    type Error = Box<dyn Error>;

    fn try_from(shell: Shell<T, M, H>) -> Result<Self, Box<dyn Error>> {
        let mut this = Self {
            commands: shell.commands,
            state: shell.state,
            handler: DefaultCommandLineHandler { proj_name: None },
            description: shell.description,
        };
        this.load_cache()?;
        Ok(this)
    }
}

impl<T: Serialize + for<'a> Deserialize<'a>> App<T, DefaultCommandLineHandler> {
    /// Creates a new shell
    pub fn new(state: T, project_name: String) -> Result<Self, Box<dyn Error>> {
        let mut this = Self {
            commands: HashMap::new(),
            state,
            handler: DefaultCommandLineHandler {
                proj_name: Some(project_name),
            },
            description: String::new(),
        };
        this.load_cache()?;
        Ok(this)
    }
}

impl<
        T: Serialize + for<'a> Deserialize<'a>,
        H: Handler<T> + CommandLineHandler,
    > App<T, H>
{
    /// Creates a new shell with the given handler.
    ///
    /// **Note that this should be a handler which can deal with cli
    /// arguments, as in the FIRST ARGUMENT is the BINARY name.**
    pub fn new_with_handler(
        state: T,
        handler: H,
    ) -> Result<Self, Box<dyn Error>> {
        let mut this = Self {
            commands: HashMap::new(),
            state,
            handler,
            description: String::new(),
        };
        this.load_cache()?;
        Ok(this)
    }

    /// Loads the state from cache.
    pub fn load_cache(&mut self) -> Result<(), Box<dyn Error>> {
        if let Some(cache) = self.handler.get_cache() {
            // Try and open the file
            if let Ok(mut file) = fs::File::open(cache) {
                // Get the string
                let mut string = String::new();
                file.read_to_string(&mut string)?;

                // Parse it with serde json
                self.state = serde_json::from_str(&string)?;
            }
        }

        Ok(())
    }

    /// Handles an vec of strings, like environment arguments.
    ///
    /// Returns a bool on wether we have 'quit' or not
    pub fn run_vec(&mut self, vec: Vec<String>) -> std::io::Result<bool> {
        let result = self.handler.handle(
            vec,
            &self.commands,
            &mut self.state,
            &self.description,
        );

        // Do stuff with the cache
        match result {
            // Delete the cache
            true => {
                if let Some(cache) = self.handler.get_cache() {
                    fs::remove_file(cache)?;
                }
            }
            // Write the cache
            false => {
                if let Some(cache) = self.handler.get_cache() {
                    // Create the dir
                    if let Some(dir) = cache.parent() {
                        fs::create_dir_all(dir)?;
                    }

                    // Create the file
                    let mut file = fs::File::create(cache)?;
                    file.write_all(
                        serde_json::to_string(&self.state)?.as_bytes(),
                    )?;
                }
            }
        }
        Ok(result)
    }

    /// Runs from the env args
    pub fn run_args(&mut self) -> std::io::Result<bool> {
        self.run_vec(env::args().collect())
    }
}
