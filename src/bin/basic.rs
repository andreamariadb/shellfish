use shellfish::{app, Command, Shell};
use std::convert::TryInto;
use std::error::Error;
use std::fmt;
use std::ops::AddAssign;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    // Define a shell
    let mut shell = Shell::new(0_u64, "<[Shellfish Example]>-$ ");

    // Add some commands
    shell.commands.insert(
        "greet".to_string(),
        Command::new("greets you.".to_string(), greet),
    );

    shell.commands.insert(
        "echo".to_string(),
        Command::new("prints the input.".to_string(), echo),
    );

    shell.commands.insert(
        "count".to_string(),
        Command::new("increments a counter.".to_string(), count),
    );

    // Check if we have > 2 args, if so no need for interactive shell
    let mut args = std::env::args();
    if args.nth(1).is_some() {
        // Create the app from the shell.
        let mut app: app::App<u64, app::DefaultCommandLineHandler> =
            shell.try_into()?;

        // Set the binary name
        app.handler.proj_name = Some("shellfish-example".to_string());
        app.load_cache()?;

        // Run it
        app.run_args()?;
    } else {
        // Run the shell
        shell.run()?;
    }
    Ok(())
}

/// Greets the user
fn greet(_state: &mut u64, args: Vec<String>) -> Result<(), Box<dyn Error>> {
    let arg = args.get(1).ok_or_else(|| Box::new(GreetingError))?;
    println!("Greetings {}, my good friend.", arg);
    Ok(())
}

/// Echos the input
fn echo(_state: &mut u64, args: Vec<String>) -> Result<(), Box<dyn Error>> {
    let mut args = args.iter();
    args.next();
    for arg in args {
        print!("{} ", arg);
    }
    println!();
    Ok(())
}

/// Acts as a counter
fn count(state: &mut u64, _args: Vec<String>) -> Result<(), Box<dyn Error>> {
    state.add_assign(1);
    println!("You have used this counter {} times", state);
    Ok(())
}

/// Greeting error
#[derive(Debug)]
pub struct GreetingError;

impl fmt::Display for GreetingError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "No name specified")
    }
}

impl Error for GreetingError {}
